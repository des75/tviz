 
$(function(){
	$(".button-show-menu").on("click", function(){
		$(".nav", $(this).parent()).toggleClass("showed");
	});
	
	var sections = $(".page");
	var navigation_links = $(".navbar a");
	
	sections.waypoint({
		handler: function(event, direction) {
		
			var active_section;
			active_section = $(this);

			if (direction == "up") active_section = active_section.prev();

			var active_link = $('.navbar a[href="#' + active_section.attr("id") + '"]');
			navigation_links.removeClass("selected");
			active_link.addClass("selected");
		},
		offset: '25%'
	})
});